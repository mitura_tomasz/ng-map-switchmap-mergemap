import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Observable, BehaviorSubject, Subject, fromEvent } from 'rxjs';
import { interval, of, from,  } from 'rxjs'; 
import { map, mergeMap, delay, mergeAll, concatMap, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

  mapInput: any = "";
  concatMapInput: any = "";
  switchMapInput: any = "";

  mergeMapNameInput: any = '';
  mergeMapSurnameInput: any = '';
  mergeMapFullNameInput: any = '';

  obsMap: Observable<any> ;
  obsConcatMap: Observable<any>;
  obsMergeMap: Observable<any>;
  obsSwitchMap: Observable<any>;

  @ViewChild('switchMapButton') switchMapButton: ElementRef;
  @ViewChild('mergeMapNameInput') mergeMapNameInputElem: ElementRef;
  @ViewChild('mergeMapSurnameInput') mergeMapSurnameInputElem: ElementRef;


  constructor() { }

  ngOnInit() { 
    this.obsMap =  from([1, 2, 3, 4, 5, 6]).pipe(
      map(item => item *= 2)
    )

    this.obsConcatMap =  from([1, 2, 3, 4, 5, 6]).pipe(
      concatMap(item => of(item).pipe(delay(1000)) )
    )
  }

  ngAfterViewInit() {



    // MERGE MAP
    fromEvent(this.mergeMapNameInputElem.nativeElement, 'keyup').pipe(
      mergeMap((s1: any) => fromEvent(this.mergeMapSurnameInputElem.nativeElement, 'keyup').pipe(
        map((s2: any) => (s1.path[0].value + ' ' + s2.path[0].value))
      ))
    ).subscribe(fullName => this.mergeMapFullNameInput = fullName)

    fromEvent(this.mergeMapSurnameInputElem.nativeElement, 'keyup').pipe(
      mergeMap((s1: any) => fromEvent(this.mergeMapNameInputElem.nativeElement, 'keyup').pipe(
        map((s2: any) => (s2.path[0].value + ' ' + s1.path[0].value))
      ))
    ).subscribe(fullName => this.mergeMapFullNameInput = fullName)

  }



  onMapBtnClick() {
    this.obsMap.subscribe(x => {
      this.mapInput += x + ' ' ;
    })
  }

  onConcatMapBtnClick() {
    this.obsConcatMap.subscribe(x => {
      this.concatMapInput += x + ' ' ;
    })
  }

  onSwitchMapBtnClick() {
    this.obsSwitchMap.subscribe(x => {
      console.log(x)
    })
  }
}
